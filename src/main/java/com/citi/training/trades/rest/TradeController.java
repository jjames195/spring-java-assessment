package com.citi.training.trades.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.service.TradeService;
/**
 * REST Interface class for {@link com.citi.training.trade.model.Trade} Trade domain object.
 * @see Trade
 * @see <a href="http://www/google.com">Google</a>
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/trades")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
 
    @Autowired
    private TradeService tradeService;

    
    
   
    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Trade> findAll(){
        LOG.debug("findAll() was called");
        return tradeService.findAll();
    }

    
    @RequestMapping(value="/{id}", method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public Trade RetreiveById(@PathVariable int id) {
        LOG.debug("findById() was called, id: " + id);
        return tradeService.findById(id);
    }
   
    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Trade> create(@RequestBody Trade trade) {
        LOG.debug("create was called, employee: " + trade);
        return new ResponseEntity<Trade>(tradeService.create(trade),
                                            HttpStatus.CREATED);
    }

    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteById(@PathVariable int id) {
        LOG.debug("deleteById was called, id: " + id);
        tradeService.deleteById(id);
    }
}