package com.citi.training.trades.controllerTests;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.rest.TradeController;
import com.citi.training.trades.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(TradeController.class)
public class ControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(ControllerTest.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeService mockTradeService;

    @Test
    public void findAllEmployees_returnsList() throws Exception {
        when(mockTradeService.findAll()).thenReturn(new ArrayList<Trade>());

        MvcResult result = this.mockMvc.perform(get("/trades")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();

        logger.info("Result from TradeDao.findAll: " +
                    result.getResponse().getContentAsString());
    }

    @Test
    public void createEmployee_returnsCreated() throws Exception {
        Trade testTrade = new Trade(5, "ABC", 99.99, 10);

        this.mockMvc
                .perform(post("/trades").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(testTrade)))
                .andExpect(status().isCreated()).andReturn();
        logger.info("Result from Create Trade");
    }

    @Test
    public void deleteTrade_returnsOK() throws Exception {
        MvcResult result = this.mockMvc.perform(delete("/trades/5"))
                                       .andExpect(status().isNoContent())
                                       .andReturn();

        logger.info("Result from TradeDao.delete: " +
                    result.getResponse().getContentAsString());
    }

    @Test
    public void getEmployeeById_returnsOK() throws Exception {
        Trade testTrade = new Trade(1, "ABC", 23.3, 10);

        when(mockTradeService.findById(testTrade.getId())).thenReturn(testTrade);

        MvcResult result = this.mockMvc.perform(get("/trades/1"))
                                       .andExpect(status().isOk()).andReturn();

        logger.info("Result from TradeDao.getEmployee: " +
                    result.getResponse().getContentAsString());
    }

}