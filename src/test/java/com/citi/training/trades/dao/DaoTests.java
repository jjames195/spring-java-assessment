package com.citi.training.trades.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.model.Trade;


public class DaoTests {

	@RunWith(SpringRunner.class)
	@SpringBootTest
	@ActiveProfiles("h2")
	public class MysqlTradeDaoTests {

	    @Autowired
	    MySqlTradeDao mysqlTradeDao;

	    @Test
	    @Transactional
	    public void test_createAndFindAll() {
	        mysqlTradeDao.create(new Trade(-1, "ABC", 10.00, 20));

	        assertEquals(1, mysqlTradeDao.findAll().size());
	    }
	
}
}
