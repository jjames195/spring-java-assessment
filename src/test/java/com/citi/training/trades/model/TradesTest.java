package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class TradesTest {

	private int testID = 1;
	private String testStock = "ABC";
	private double testPrice = 12.00;
	private int testVolume= 10;
	
	
	
	 @Test
	    public void test_Trade_constructor() {
	        Trade testTrade = new Trade(testID, testStock, testPrice, testVolume);

	        assertEquals(testID, testTrade.getId());
	        assertEquals(testStock, testTrade.getStock());
	        assertEquals(testPrice, testTrade.getPrice(), 0.0001);
	        assertEquals(testVolume, testTrade.getVolume());

	    }

	    @Test
	    public void test_Trade_toString() {
	        String testString = new Trade(testID, testStock, testPrice, testVolume).toString();

	        assertTrue(testString.contains((new Integer(testID)).toString()));
	        assertTrue(testString.contains(testStock));
	        assertTrue(testString.contains(String.valueOf(testPrice)));
	        assertTrue(testString.contains(String.valueOf(testVolume)));

	    }
}
